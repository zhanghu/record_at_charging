package com.example.usblistendemo;

import java.io.File;
import java.util.Date;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Toast;

@SuppressWarnings("deprecation")
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class BackgroundVideoRecorder extends Service implements
		SurfaceHolder.Callback {

	private WindowManager windowManager;
	private SurfaceView surfaceView;
	private Camera camera = null;
	private MediaRecorder mediaRecorder = null;
	private Sensor sensor;
	private SensorManager sensorManager;
	private float lux;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
	public void onCreate() {
		Notification notification = new Notification.Builder(this)
				.setContentTitle("Background Video Recorder")
				.setContentText("").setSmallIcon(R.drawable.ic_launcher)
				.build();
		startForeground(1234, notification);

		windowManager = (WindowManager) this
				.getSystemService(Context.WINDOW_SERVICE);
		surfaceView = new SurfaceView(this);
		LayoutParams layoutParams = new WindowManager.LayoutParams(1, 1,
				WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
				WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
				PixelFormat.TRANSLUCENT);
		layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
		windowManager.addView(surfaceView, layoutParams);
		surfaceView.getHolder().addCallback(this);

		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

		sensorManager.registerListener(new SensorEventListener() {

			@Override
			public void onSensorChanged(SensorEvent event) {
				lux = event.values[0];
			}

			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy) {

			}
		}, sensor, SensorManager.SENSOR_DELAY_NORMAL);
	}

	// Method called right after Surface created (initializing and starting
	// MediaRecorder)
	@SuppressLint("NewApi")
	@Override
	public void surfaceCreated(SurfaceHolder surfaceHolder) {

		camera = Camera.open();
		SharedPreferences pre = PreferenceManager.getDefaultSharedPreferences(this);
		float maxLux = Float.valueOf(pre.getString("lux_value", "20.0"));
		boolean flashLightEnable = pre.getBoolean("flash_light_open", true);
		if (lux <= maxLux && flashLightEnable) {
			Camera.Parameters mParameters;
			mParameters = camera.getParameters();
			mParameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
			camera.setParameters(mParameters);
		}

		mediaRecorder = new MediaRecorder();
		camera.unlock();

		mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
		mediaRecorder.setCamera(camera);
		mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
		mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
		mediaRecorder.setProfile(CamcorderProfile
				.get(CamcorderProfile.QUALITY_480P));
		mediaRecorder.setVideoSize(640, 480);
		File file = new File(Environment.getExternalStorageDirectory()
						+ "/"
						+ "recordWhenCharging/");
		if (! file.exists()) {
			file.mkdir();
		}
		mediaRecorder
				.setOutputFile(Environment.getExternalStorageDirectory()
						+ "/"
						+ "recordWhenCharging/"
						+ "rec"
						+ DateFormat.format("yyyy-MM-dd_kk-mm-ss",
								new Date().getTime()) + ".mp4");
		try {
			mediaRecorder.prepare();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Toast.makeText(getApplicationContext(), "start record",
				Toast.LENGTH_SHORT).show();
		mediaRecorder.start();

	}

	// Stop recording and remove SurfaceView
	@Override
	public void onDestroy() {
		Toast.makeText(getApplicationContext(), "stop record",
				Toast.LENGTH_SHORT).show();
		mediaRecorder.stop();
		mediaRecorder.reset();
		mediaRecorder.release();

		camera.lock();
		camera.release();

		windowManager.removeView(surfaceView);

	}

	@Override
	public void surfaceChanged(SurfaceHolder surfaceHolder, int format,
			int width, int height) {
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}
