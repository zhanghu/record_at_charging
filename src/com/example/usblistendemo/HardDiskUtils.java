package com.example.usblistendemo;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

public class HardDiskUtils {
	public static final String TAG = "HardDiskUtils";
	
	public static long HADR_DISK_SPACE_MINIMUM = 1024;

	@SuppressWarnings("deprecation")
	public static long getMinimunSpaceToSupportEnoughSpace() {
		File root = Environment.getExternalStorageDirectory();
		StatFs sf = new StatFs(root.getPath());
		long blockSize = sf.getBlockSize();
		long availCount = sf.getAvailableBlocks();
		long availMB = (availCount * blockSize) / 1024 / 1024;
		if ( availMB < HADR_DISK_SPACE_MINIMUM) {
			return HADR_DISK_SPACE_MINIMUM - availMB;
		}
		return 0;
	}
	
	public static void deleteRecordFilesToSupportEnoughSpace(Context ctx) {
		long needMinimunSpace = getMinimunSpaceToSupportEnoughSpace();
		if (needMinimunSpace <= 0) {
			return;
		}
		List<FileInfo> orderedFileInfoList = getRecordFileInfoOrderByDate();
		Iterator<FileInfo> fileInfoIterator = orderedFileInfoList.iterator();
		while(fileInfoIterator.hasNext() && needMinimunSpace > 0) {
			FileInfo fileInfo = fileInfoIterator.next();
			String path =  fileInfo.filePath;
			long length = fileInfo.length;
			length = length / 1024 / 1024;
			File toDelFile = new File(path);
			Log.v(TAG, "delete the file : " + path);
			toDelFile.delete();
			needMinimunSpace -= length;
		}
	}

	private static List<FileInfo> getRecordFileInfoOrderByDate() {
		String cachePath = Environment.getExternalStorageDirectory() + "/recordWhenCharging/";
		File pfile = new File(cachePath);
		if (!pfile.exists()) {
			pfile.mkdir();
		}
		File[] files = pfile.listFiles();
		ArrayList<FileInfo> fileList = new ArrayList<FileInfo>();// 将需要的子文件信息存入到FileInfo里面
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			
			FileInfo fileInfo = new FileInfo();
			fileInfo.length = file.length();
			fileInfo.fileName = file.getName();
			fileInfo.filePath = file.getPath();
			fileInfo.lastModified = file.lastModified();
			fileList.add(fileInfo);
		}
		Collections.sort(fileList, recordFileCompareByDate);
		return fileList;
	}

	private static FileFilter recordFileFilter = new FileFilter() {

		@Override
		public boolean accept(File pathname) {
			String fileName = pathname.getName().toLowerCase();
			return fileName.endsWith(".mp4") && fileName.startsWith("rec");
		}
	};

	private static Comparator<FileInfo> recordFileCompareByDate = new Comparator<FileInfo>() {

		@Override
		public int compare(FileInfo lhs, FileInfo rhs) {
			if (lhs.lastModified < rhs.lastModified) {
				return -1;
			} else {
				return 1;
			}
		}
	};

	private static class FileInfo {
		public String fileName;
		public String filePath;
		public long lastModified;
		public long length;
	}
}
